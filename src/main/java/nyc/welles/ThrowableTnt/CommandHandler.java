package nyc.welles.ThrowableTnt;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class CommandHandler implements CommandExecutor {

//    private final void saveConfig;
    private JavaPlugin javaPlugin;

    public CommandHandler(JavaPlugin plugin) {
        this.javaPlugin = plugin;
    }
//    FileConfiguration config = (this.javaPlugin).getConfig();
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (player.hasPermission("throwabletnt.toggle")) {
                //player.sendMessage("Reached onCommand");
                this.javaPlugin.saveConfig();
                boolean tntToggle = this.javaPlugin.getConfig().getBoolean("data." + player.getUniqueId().toString() + ".enabled");
                if (tntToggle) {
                    player.sendMessage("§9[§r§eThrowableTNT§9]§f You will no longer throw TNT");
                } else {
                    player.sendMessage("§9[§r§eThrowableTNT§9]§f You will now be able to throw TNT");

                }
                this.javaPlugin.getConfig().set("data." + player.getUniqueId().toString() + ".enabled", !tntToggle);
                javaPlugin.saveConfig();
                return true;
            }
            else{
                player.sendMessage("§9[§r§eThrowableTNT§9]§c You don't have the permissions to perform this command");
            }

        }
        return false;
    }

}
