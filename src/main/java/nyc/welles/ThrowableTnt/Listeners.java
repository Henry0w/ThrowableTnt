package nyc.welles.ThrowableTnt;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Time;
import java.util.HashMap;
import java.util.UUID;

public class Listeners implements Listener {
    private JavaPlugin javaPlugin;
    private HashMap<UUID, Long> uuidLastThrownMap = new HashMap<>();

    public Listeners(JavaPlugin plugin) {
        this.javaPlugin = plugin;
    }
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent playerJoinEvent){
        Player player = playerJoinEvent.getPlayer();
        if (!this.javaPlugin.getConfig().contains("data." + player.getUniqueId().toString() + ".enabled")){
            this.javaPlugin.getConfig().set("data." + player.getUniqueId().toString() + ".enabled", true);
        }

    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent playerInteractEvent) {
        Player player = playerInteractEvent.getPlayer();
        if (this.javaPlugin.getConfig().getBoolean("worlds." + player.getWorld().getName() + ".enabled")) {
            if (this.javaPlugin.getConfig().getBoolean("data." + player.getUniqueId().toString() + ".enabled")){
                ItemStack airStack = new ItemStack(Material.AIR);
                if (playerInteractEvent.isBlockInHand() && playerInteractEvent.getMaterial() == Material.TNT) {
                    if (playerInteractEvent.getAction() == Action.RIGHT_CLICK_AIR || playerInteractEvent.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        ItemStack itemStack = player.getInventory().getItemInMainHand();
                        UUID playerUuid = player.getUniqueId();
                        playerInteractEvent.setCancelled(true);
                        long now = System.currentTimeMillis();
                        int cooldown = javaPlugin.getConfig().getInt("cooldown(milliseconds)");
                        if (this.uuidLastThrownMap.containsKey(playerUuid)) {
                            //System.out.println("uuidLastThrownMap contains player uuid");
                            long lastTimeThrown = uuidLastThrownMap.get(playerUuid);
                            /*String s = "cooldown is " + cooldown +
                                    ", lastTimeThrown is " + lastTimeThrown +
                                    ", cooldown plus last time thrown is " + (cooldown + lastTimeThrown)  +
                                    ", current time is " + now +
                                    ", lastTimeThrown + javaPlugin.getConfig().getInt(\"cooldown(milliseconds)\") <= now returns " + ChatColor.BLUE + ChatColor.ITALIC + (lastTimeThrown + cooldown <= now) +
                                    ChatColor.RESET + ", current time minus lastTimeThrown + cooldown is " + (now - (lastTimeThrown + cooldown)
                            );
                            player.sendMessage(s);
                            System.out.println(s);*/
                            if (lastTimeThrown + cooldown <= now) {
                                this.uuidLastThrownMap.replace(playerUuid, now);
                                removeTnt(player, airStack, itemStack);
                                TNTPrimed tnt = player.getWorld().spawn(player.getLocation(), TNTPrimed.class);
                                tnt.setVelocity(player.getLocation().getDirection().normalize());
                            }


                        } else {
                            removeTnt(player, airStack, itemStack);
                            TNTPrimed tnt = player.getWorld().spawn(player.getLocation(), TNTPrimed.class);
                            tnt.setVelocity(player.getLocation().getDirection().normalize());

                        }
                        if (!this.uuidLastThrownMap.containsKey(playerUuid)) {
                            this.uuidLastThrownMap.put(player.getUniqueId(), now);
                        }


                        //javaPlugin.saveConfig();


                    }

                }
            }

        }else{
            player.sendMessage("§9[§r§eThrowableTNT§9]§f You cannot throw TNT in this world!");
        }
        if (this.javaPlugin.getConfig().getStringList("enabled-worlds").contains(player.getWorld().getName())) {

        }


    }

    private void removeTnt(Player player, ItemStack airStack, ItemStack itemStack) {
        if (player.getGameMode() != GameMode.CREATIVE) {
            if (itemStack.getAmount() != 1 && itemStack.getAmount() != 0) {
                itemStack.setAmount(itemStack.getAmount() - 1);
                player.getInventory().setItemInMainHand(itemStack);
            } else {
                player.getInventory().setItemInMainHand(airStack);
            }
        }
    }
}
