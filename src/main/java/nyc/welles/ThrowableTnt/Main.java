package nyc.welles.ThrowableTnt;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Main extends JavaPlugin {
    FileConfiguration config = getConfig();
    @Override
    public void onEnable(){
        config.addDefault("enabled", true);
        config.addDefault("cooldown(milliseconds)", 0);

        for (World world : Bukkit.getWorlds()){
            config.addDefault("worlds." + world.getName() + ".enabled", true);
        }
        config.options().copyDefaults(true);
        saveConfig();
        if (getConfig().getBoolean("enabled")){
            getServer().getPluginManager().registerEvents(new Listeners(this), this);
            Objects.requireNonNull(this.getCommand("tnttoggle")).setExecutor(new CommandHandler(this));
        }
    }
}
