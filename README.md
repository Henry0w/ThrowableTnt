# ThrowableTnt
A plugin for my reddit plugin giveaway!

**Description**

This plugin allows players to throw TNT when they right click with TNT in their main hand.

**Permissions**
- `throwabletnt.toggle` -- allow players to use /tnttoggle: by default they throw TNT

**Commands**
- `/tnttoggle` -- toggle if you throw TNT or place TNT like normal
